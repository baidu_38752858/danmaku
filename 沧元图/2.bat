@echo off
 
Setlocal Enabledelayedexpansion
 
set "str= "
 
for /f "delims=" %%i in ('dir /b *.*') do (
 
set "var=%%i" & ren "%%i" "!var:%str%=!")

for /L %%I in (1,1,93) do (
if %%I LSS 10 (
	ren %%I.xml E0%%I.xml
) else (
	ren %%I.xml E%%I.xml
)
echo %%I
)
pause